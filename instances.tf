resource "random_id" "random_bytes" {
  byte_length = 4
}

resource "google_compute_instance" "nginx" {
  count        = var.node_count
  name         = "nginx-${count.index}-${random_id.random_bytes.hex}"
  machine_type = "f1-micro"
  tags         = ["http"]
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }
  network_interface {
    network = "default"
    access_config {
      nat_ip = ""
    }
  }

  provisioner "file" {
    connection {
      type        = var.remote_exec.conn_type
      user        = var.remote_exec.username
      host        = self.network_interface[0].access_config[0].nat_ip
      private_key = file(var.remote_exec.priv_key_path)
    }

    source      = "init.sh"
    destination = "/tmp/init.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = var.remote_exec.conn_type
      user        = var.remote_exec.username
      host        = self.network_interface[0].access_config[0].nat_ip
      private_key = file(var.remote_exec.priv_key_path)
    }
    inline = [
      "chmod +x /tmp/init.sh",
      "/tmp/init.sh ${self.name} ${self.network_interface[0].network_ip}",
    ]
  }

  attached_disk {
    source      = element(google_compute_disk.nginx-disk-1-.*.self_link, count.index)
    device_name = element(google_compute_disk.nginx-disk-1-.*.name, count.index)
  }

  attached_disk {
    source      = element(google_compute_disk.nginx-disk-2-.*.self_link, count.index)
    device_name = element(google_compute_disk.nginx-disk-2-.*.name, count.index)
  }

  lifecycle {
    ignore_changes = [attached_disk]
  }
  allow_stopping_for_update = true
}

resource "google_compute_disk" "nginx-disk-1-" {
  count = var.node_count
  name  = "nginx-disk-${count.index}-1"
  type  = "pd-standard"
  size  = 10
}

resource "google_compute_disk" "nginx-disk-2-" {
  count = var.node_count
  name  = "nginx-disk-${count.index}-2"
  type  = "pd-standard"
  size  = 8
}
