output "balancer_ip_address" {
  value = google_compute_forwarding_rule.webserver.ip_address
}

output "balancer_hostname" { 
  value = var.balancer_hostname
}