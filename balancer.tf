resource "google_compute_address" "external-ip-balancer" {
  name = "external-ip-${random_id.random_bytes.hex}"
}

resource "cloudflare_record" "balancer-a-record" {
  zone_id = var.cloudflare.zone_id
  name    = var.balancer_hostname
  value   = google_compute_address.external-ip-balancer.address
  type    = "A"
  ttl     = 300
}

resource "google_compute_instance_group" "webservers" {
  name      = "webservers"
  instances = google_compute_instance.nginx[*].self_link

  named_port {
    name = "http"
    port = "80"
  }
}

resource "google_compute_forwarding_rule" "webserver" {
  name            = "website-forwarding"
  port_range      = 80
  backend_service = google_compute_region_backend_service.backend.id
  ip_address      = google_compute_address.external-ip-balancer.id
}

resource "google_compute_region_backend_service" "backend" {
  name                  = "website-backend"
  protocol              = "TCP"
  load_balancing_scheme = "EXTERNAL"

  health_checks = [google_compute_region_health_check.web-index.id]

  backend {
    balancing_mode = "CONNECTION"
    group          = google_compute_instance_group.webservers.id
  }
}
resource "google_compute_region_health_check" "web-index" {
  name               = "check-website-index"
  check_interval_sec = 1
  timeout_sec        = 1

  http_health_check {
    port         = "80"
    request_path = "/"
  }
}