#!/usr/bin/env sh

instance_name=$1
internal_ip=$2

sudo apt update && sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io -y

echo "Juneway ${instance_name} ${internal_ip}" | sudo tee /root/index.html
sudo docker run -d -p 80:80 -v /root/index.html:/usr/share/nginx/html/index.html nginx