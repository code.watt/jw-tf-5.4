variable "google" {
  type = object({
    project     = string
    region      = string
    zone        = string
    credentials = string
  })
}

variable "cloudflare" {
  type = object({
    email      = string
    api_key    = string
    account_id = string
    zone_id    = string
  })
}


variable "balancer_hostname" {
  type    = string
  default = "creit0r.juneway.pro"
}

variable "remote_exec" {
  type = object({
    conn_type     = string
    username      = string
    priv_key_path = string
  })
  default = {
    conn_type     = "ssh"
    username      = "root"
    priv_key_path = "~/.ssh/id_rsa"
  }
}

variable "node_count" {
  default = "2"
}